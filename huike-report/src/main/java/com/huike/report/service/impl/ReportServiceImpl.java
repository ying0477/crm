package com.huike.report.service.impl;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.CompletableFuture;

import com.huike.clues.domain.TbCourse;
import com.huike.clues.service.ITbCourseService;
import com.huike.common.core.domain.AjaxResult;
import com.huike.common.utils.DateUtils;
import com.huike.contract.service.ITbContractService;
import com.huike.report.domain.vo.*;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.huike.business.mapper.TbBusinessMapper;
import com.huike.clues.domain.TbActivity;
import com.huike.clues.domain.TbAssignRecord;
import com.huike.clues.domain.TbClue;
import com.huike.clues.domain.vo.IndexStatisticsVo;
import com.huike.clues.mapper.SysDeptMapper;
import com.huike.clues.mapper.SysDictDataMapper;
import com.huike.clues.mapper.TbActivityMapper;
import com.huike.clues.mapper.TbAssignRecordMapper;
import com.huike.clues.mapper.TbClueMapper;
import com.huike.common.core.domain.entity.SysDept;
import com.huike.common.utils.SecurityUtils;
import com.huike.contract.domain.TbContract;
import com.huike.contract.mapper.TbContractMapper;
import com.huike.report.mapper.ReportMapper;
import com.huike.report.service.IReportService;

@Service
public class ReportServiceImpl implements IReportService {

    @Autowired
    private TbContractMapper contractMapper;


    @Autowired
    private SysDictDataMapper sysDictDataMapper;


    @Autowired
    private TbClueMapper clueMapper;

    @Autowired
    private TbActivityMapper activityMapper;

    @Autowired
    private TbBusinessMapper businessMapper;

    @Autowired
    private SysDeptMapper deptMapper;

    @Autowired
    private TbAssignRecordMapper assignRecordMapper;

    @Autowired
    private ReportMapper reportMpper;

    @Autowired
    private ITbContractService tbContractService;

    @Autowired
    private ITbCourseService tbCourseService;

    @Override
    public LineChartVO contractStatistics(String beginCreateTime, String endCreateTime) {
        //创建折现对象
        LineChartVO lineChartVo =new LineChartVO();
        try {
            //获取所有时间点
            List<String> timeList= findDates(beginCreateTime,endCreateTime);
            //存入折现对象的返回结果
            lineChartVo.setxAxis(timeList);
            //创建集合，集合中是折线点
            List<LineSeriesVO> series = new ArrayList<>();
            //统计单点时间内合同数
            List<Map<String,Object>> statistics = contractMapper.contractStatistics(beginCreateTime,endCreateTime);
            //创建
            LineSeriesVO lineSeriesDTO1=new LineSeriesVO();
            //将新增客户数加入
            lineSeriesDTO1.setName("新增客户数");
            //创建
            LineSeriesVO lineSeriesDTO2=new LineSeriesVO();
            //将客户总数加入
            lineSeriesDTO2.setName("客户总数");
            int sum = 0;
            for (String s : timeList) {
                //流的写法，遍历时间点和合同的Create_time相等的列-----看sql别名可知
                Optional optional=  statistics.stream().filter(d->d.get("dd").equals(s)).findFirst();
                //查看对象中是否有一个值，有值就取出然后存入线1新增客户数
                if(optional.isPresent()){
                    Map<String,Object> cuurentData=  (Map<String,Object>)optional.get();
                        lineSeriesDTO1.getData().add(cuurentData.get("num"));
                    sum += Integer.parseInt(cuurentData.get("num").toString());
                }else{
                    lineSeriesDTO1.getData().add(0);
                }
                //将所求和存入线2客户总数
                lineSeriesDTO2.getData().add(sum);
            }
            //将结果规范化
            series.add(lineSeriesDTO1);
            series.add(lineSeriesDTO2);
            lineChartVo.setSeries(series);
        } catch (ParseException e) {
            // e.printStackTrace();
        }
        return  lineChartVo;
    }

    @Override
    public LineChartVO salesStatistics(String beginCreateTime, String endCreateTime) {
        LineChartVO lineChartVo =new LineChartVO();
        try {
            List<String> timeList= findDates(beginCreateTime,endCreateTime);
            lineChartVo.setxAxis(timeList);
            List<LineSeriesVO> series = new ArrayList<>();
            List<Map<String,Object>>  statistics = contractMapper.salesStatistics(beginCreateTime,endCreateTime);
            LineSeriesVO lineSeriesVo=new LineSeriesVO();
            lineSeriesVo.setName("销售统计");
            int sum=0;
            for (String s : timeList) {
                Optional optional=  statistics.stream().filter(d->d.get("dd").equals(s)).findFirst();
                if(optional.isPresent()){
                    Map<String,Object> cuurentData=  (Map<String,Object>)optional.get();
                    lineSeriesVo.getData().add(cuurentData.get("sales"));
                }else{
                    lineSeriesVo.getData().add(0);
                }
            }
            series.add(lineSeriesVo);
            lineChartVo.setSeries(series);
        } catch (ParseException e) {
            // e.printStackTrace();
        }
        return  lineChartVo;
    }




    /**
     * 渠道统计
     */
    @Override
    public List<Map<String, Object>> chanelStatistics(String beginCreateTime, String endCreateTime) {
        List<Map<String, Object>> data= contractMapper.chanelStatistics(beginCreateTime,endCreateTime);
        for (Map<String, Object> datum : data) {
            String subjectValue= (String) datum.get("channel");
            String lable=  sysDictDataMapper.selectDictLabel("clues_item",subjectValue);
            datum.put("channel",lable);
        }
        return data;
    }

    @Override
    public List<TbContract> contractReportList(TbContract tbContract) {
        return contractMapper.selectTbContractList(tbContract);
    }


    @Override
    public List<Map<String, Object>> activityStatistics(String beginCreateTime, String endCreateTime) {
        List<Map<String, Object>> data= contractMapper.activityStatistics(beginCreateTime,endCreateTime);
        for (Map<String, Object> datum : data) {
            Long activityId= (Long) datum.get("activity_id");
            TbActivity tbActivity = activityMapper.selectTbActivityById(activityId);
            if(tbActivity==null){
                datum.put("activity", "其他");
            }else{
                datum.put("activity", tbActivity.getName());
            }
        }
        return data;
    }

    /**
     * 按照部门统计销售
     * @param beginCreateTime
     * @param endCreateTime
     * @return
     */
    @Override
    public List<Map<String, Object>> deptStatisticsList(String beginCreateTime, String endCreateTime) {
        List<Map<String, Object>> data= contractMapper.deptStatistics(beginCreateTime,endCreateTime);
        for (Map<String, Object> datum : data) {
            Long deptId= (Long) datum.get("dept_id");
            if(deptId!=null){
                SysDept dept= deptMapper.selectDeptById(deptId);
                datum.put("deptName", dept.getDeptName());
            }
        }
        return data;
    }


    /**
     * 按照渠道统计销售
     * @param beginCreateTime
     * @param endCreateTime
     * @return
     */
    @Override
    public List<Map<String, Object>> channelStatisticsList(String beginCreateTime, String endCreateTime) {
        List<Map<String, Object>> data= contractMapper.channelStatistics(beginCreateTime,endCreateTime);
        for (Map<String, Object> datum : data) {
            String subjectValue= (String) datum.get("channel");
            if(subjectValue!=null){
                String lable=  sysDictDataMapper.selectDictLabel("clues_item",subjectValue);
                datum.put("channel",lable);
            }
        }
        return data;
    }


    /**
     * 按照归属人统计销售
     * @param beginCreateTime
     * @param endCreateTime
     * @return
     */
    @Override
    public List<Map<String, Object>> ownerShipStatisticsList(String beginCreateTime, String endCreateTime) {
        return  contractMapper.ownerShipStatistics(beginCreateTime,endCreateTime);
    }


    @Override
    public List<TbClue> cluesStatisticsList(TbClue clue) {
        return clueMapper.selectTbClueForReport(clue);
    }

    @Override
    public List<ActivityStatisticsVo> activityStatisticsList(TbActivity query) {
        query.setStatus("2");
        List<TbActivity> activities= activityMapper.selectTbActivityList(query);
        Map<String, Object> timeMap = query.getParams();
        List<ActivityStatisticsVo> list=new ArrayList<>();
        for (TbActivity activity : activities) {
            ActivityStatisticsVo dto = new ActivityStatisticsVo();
            BeanUtils.copyProperties(activity, dto);
            TbClue tbClue = new TbClue();
            tbClue.setActivityId(activity.getId());
            tbClue.setChannel(activity.getChannel());
            tbClue.setParams(timeMap);
            Map<String, Object> clueCount = clueMapper.countByActivity(tbClue);
            if (clueCount != null) {
                dto.setCluesNum(Integer.parseInt(clueCount.get("total").toString()));
                if(clueCount.get("falseClues")!=null){
                    dto.setFalseCluesNum(Integer.parseInt(clueCount.get("falseClues").toString()));
                }
                if (clueCount.get("toBusiness") != null) {
                    dto.setBusinessNum(Integer.parseInt(clueCount.get("toBusiness").toString()));
                }
            }
            TbContract tbContract = new TbContract();
            tbContract.setChannel(activity.getChannel());
            tbContract.setActivityId(activity.getId());
            tbContract.setParams(timeMap);
            Map<String, Object> contractCount = contractMapper.countByActivity(tbContract);
            if (contractCount != null) {
                dto.setCustomersNum(Integer.parseInt(contractCount.get("customersNum").toString()));
                if(contractCount.get("amount")==null) {
                    dto.setAmount(0d);
                }else {
                    dto.setAmount((Double) contractCount.get("amount"));
                }
                if(contractCount.get("cost")==null) {
                    dto.setCost(0d);
                }else {
                    dto.setCost((Double) contractCount.get("cost"));
                }

            }
            list.add(dto);
        }
        return list;
    }

    /**
     * *************看我看我**************
     * 传入两个时间范围，返回这两个时间范围内的所有时间，并保存在一个集合中
     * @param beginTime
     * @param endTime
     * @return
     * @throws ParseException
     */
    public static List<String> findDates(String beginTime, String endTime)
            throws ParseException {
        List<String> allDate = new ArrayList();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

        Date dBegin = sdf.parse(beginTime);
        Date dEnd = sdf.parse(endTime);
        allDate.add(sdf.format(dBegin));
        Calendar calBegin = Calendar.getInstance();
        // 使用给定的 Date 设置此 Calendar 的时间
        calBegin.setTime(dBegin);
        Calendar calEnd = Calendar.getInstance();
        // 使用给定的 Date 设置此 Calendar 的时间
        calEnd.setTime(dEnd);
        // 测试此日期是否在指定日期之后
        while (dEnd.after(calBegin.getTime())) {
            // 根据日历的规则，为给定的日历字段添加或减去指定的时间量
            calBegin.add(Calendar.DAY_OF_MONTH, 1);
            allDate.add(sdf.format(calBegin.getTime()));
        }
        System.out.println("时间==" + allDate);
        return allDate;
    }


    @Override
    public IndexVo getIndex(IndexStatisticsVo request) {
        Long deptId= request.getDeptId();
        TbAssignRecord tbAssignRecord=new TbAssignRecord();
        tbAssignRecord.setLatest("1");
        assignRecordMapper.selectAssignRecordList(tbAssignRecord);
        return null;
    }

    @Override
    public List<Map<String, Object>> salesStatisticsForIndex(IndexStatisticsVo request) {
        List<Map<String, Object>> list= contractMapper.contractStatisticsByUser(request);
        for (Map<String, Object> datum : list) {
            Long deptId= (Long) datum.get("dept_id");
            if(deptId!=null){
                SysDept dept= deptMapper.selectDeptById(deptId);
                datum.put("deptName", dept.getDeptName());
            }
        }
        return list;
    }


    /**
     * ************看我看我***********
     * 用我能少走很多路
     * 我是用来机选百分比的方法
      * @param all  分母
     * @param num   分子
     * @return
     */
    private BigDecimal getRadio(Integer all,Long num) {
        if(all.intValue()==0){
            return new BigDecimal(0);
        }
        BigDecimal numBigDecimal = new BigDecimal(num);
        BigDecimal allBigDecimal = new BigDecimal(all);
        BigDecimal divide = numBigDecimal.divide(allBigDecimal,4,BigDecimal.ROUND_HALF_UP);
        return divide.multiply(new BigDecimal(100));
    }


    /**
     * 获取首页基本数据
     * @param beginCreateTime
     * @param endCreateTime
     * @return
     */
    @Override
    public IndexBaseInfoVO getBaseInfo(String beginCreateTime, String endCreateTime) {
        //1）构建一个空的结果集对象
        IndexBaseInfoVO result = new IndexBaseInfoVO();
        //2 封装结果集属性
        // 2.1 由于查询需要用到用户名 调用工具类获取用户名
        String username = SecurityUtils.getUsername();
        try {
            //3 封装结果集对象
            result.setCluesNum(reportMpper.getCluesNum(beginCreateTime, endCreateTime, username));
            result.setBusinessNum(reportMpper.getBusinessNum(beginCreateTime, endCreateTime, username));
            result.setContractNum(reportMpper.getContractNum(beginCreateTime, endCreateTime, username));
            result.setSalesAmount(reportMpper.getSalesAmount(beginCreateTime, endCreateTime, username));
        }catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        //4 返回结果集对象
        return result;
    }

    /**
     * 今日简报
     * @return
     */
    @Override
    public IndexTodayInfoVO getTodayInfo() {
        //1、创建结果集
        IndexTodayInfoVO result = new IndexTodayInfoVO();

        //2、创建查询需要的的东西
        String username = SecurityUtils.getUsername();
        String dateTimeNow = DateUtils.dateTime(new Date());
        //3、封装结果集对象
        try {
            result.setTodayCluesNum(reportMpper.getCluesNum(dateTimeNow,dateTimeNow,username));
            result.setTodayContractNum(reportMpper.getContractNum(dateTimeNow,dateTimeNow,username));
            result.setTodayBusinessNum(reportMpper.getBusinessNum(dateTimeNow,dateTimeNow,username));
            result.setTodaySalesAmount(reportMpper.getSalesAmount(dateTimeNow,dateTimeNow,username));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        return result;
    }

    /**
     * 待办事项
     * @param beginCreateTime
     * @param endCreateTime
     * @return
     */
    @Override
    public IndexTodoInfoVO getTodoInfo(String beginCreateTime, String endCreateTime) {
        //1、创建结果集
        IndexTodoInfoVO result = new IndexTodoInfoVO();

        //2、创建查询需要的的东西
        String username = SecurityUtils.getUsername();
        //3、封装结果集对象
        try {
            result.setTofollowedCluesNum(reportMpper.getTodoBusinessDGJ(beginCreateTime,endCreateTime,username));
            result.setTofollowedBusinessNum(reportMpper.getTodoBusinessDGJ(beginCreateTime,endCreateTime,username));
            result.setToallocatedCluesNum(reportMpper.getTodoCluesDFP(beginCreateTime,endCreateTime,username));
            result.setToallocatedBusinessNum(reportMpper.getTodoBusinessDFP(beginCreateTime,endCreateTime,username));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        return result;
    }

    //想法1：不重写mapper直接调用各层service，可以实现，但是卡在计数了
    /*@Override
    public List<Map<String,Object>> subjectStatistics(String beginCreateTime, String endCreateTime) {
        Map<String, Object> map = new HashMap<>();
        TbContract tbContract = new TbContract();
        tbContract.getParams().put("beginCreateTime",beginCreateTime);
        tbContract.getParams().put("endCreateTime",endCreateTime);
        //这里是合同数据
        List<TbContract> tbContracts = tbContractService.selectTbContract(tbContract);
        for (TbContract contract : tbContracts) {
            Long courseId = contract.getCourseId();
            TbCourse tbCourse = tbCourseService.selectTbCourseById(courseId);
            map.put("subject",tbCourse.getSubject());
            map.put("num",Util)
        }
        //asd
        return null;
    }*/
    @Override
    public List<Map<String,Object>> subjectStatistics(String beginCreateTime, String endCreateTime){
        return reportMpper.subjectStatistics(beginCreateTime,endCreateTime);
    }

    @Override
    public LineChartVO cluesStatistics(String beginCreateTime, String endCreateTime) {
        //创建折现对象
        LineChartVO lineChartVo =new LineChartVO();
        try {
            //获取所有时间点
            List<String> timeList= findDates(beginCreateTime,endCreateTime);
            //存入折现对象的返回结果
            lineChartVo.setxAxis(timeList);
            //创建集合，集合中是折线点
            List<LineSeriesVO> series = new ArrayList<>();
            //统计单点时间内合同数
            List<Map<String,Object>> statistics = clueMapper.clueStatistics(beginCreateTime,endCreateTime);
            //创建
            LineSeriesVO lineSeriesDTO1=new LineSeriesVO();
            //将新增客户数加入
            lineSeriesDTO1.setName("新增线索数量");
            //创建
            LineSeriesVO lineSeriesDTO2=new LineSeriesVO();
            //将客户总数加入
            lineSeriesDTO2.setName("线索总数量");
            int sum = 0;
            for (String s : timeList) {
                //流的写法，遍历时间点和合同的Create_time相等的列-----看sql别名可知
                Optional optional=  statistics.stream().filter(d->d.get("dd").equals(s)).findFirst();
                //查看对象中是否有一个值，有值就取出然后存入线1新增客户数
                if(optional.isPresent()){
                    Map<String,Object> cuurentData=  (Map<String,Object>)optional.get();
                    lineSeriesDTO1.getData().add(cuurentData.get("num"));
                    sum += Integer.parseInt(cuurentData.get("num").toString());
                }else{
                    lineSeriesDTO1.getData().add(0);
                }
                //将所求和存入线2客户总数
                lineSeriesDTO2.getData().add(sum);
            }
            //将结果规范化
            series.add(lineSeriesDTO1);
            series.add(lineSeriesDTO2);
            lineChartVo.setSeries(series);
        } catch (ParseException e) {
            // e.printStackTrace();
        }
        return  lineChartVo;
    }

    @Override
    public VulnerabilityMapVo getVulnerabilityMap(String beginCreateTime, String endCreateTime) {
        VulnerabilityMapVo result = new VulnerabilityMapVo();
        try {
            result.setBusinessNums(reportMpper.getBusinessNums(beginCreateTime,endCreateTime));
            result.setCluesNums(reportMpper.getCluesNums(beginCreateTime,endCreateTime));
            result.setContractNums(reportMpper.getContractNums(beginCreateTime,endCreateTime));
            result.setEffectiveCluesNums(reportMpper.getEffectiveCluesNums(beginCreateTime,endCreateTime));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    //商机龙虎榜1
    /*@Override
    public List<Map<String, Object>> businessChangeStatistics(String beginCreateTime, String endCreateTime) {
        List<Map<String, Object>> result = reportMpper.businessChangeStatistics(beginCreateTime, beginCreateTime);
        Integer businessNums = reportMpper.getBusinessNums(beginCreateTime, beginCreateTime);
        for (Map<String, Object> map : result) {
            Long num = (Long)map.get("num");
            BigDecimal radio = this.getRadio(businessNums, num);
            map.put("radio",radio);
        }
        return result;
    }*/

    /**
     * 商机转换龙虎榜2
     * @param beginCreateTime
     * @param endCreateTime
     * @return
     */
    @Override
    public List<Map<String, Object>> getBusinessChangeStatistics(String beginCreateTime, String endCreateTime) {
        List<Map<String, Object>> businessChangeStatistics = reportMpper.getBusinessChangeStatistics(beginCreateTime, endCreateTime);

        //获取商机总数
        Integer businessNums = reportMpper.getBusinessNums(beginCreateTime, endCreateTime);
        //遍历查询到的集合，为redio赋值
        for (Map<String, Object> businessChangeStatistic : businessChangeStatistics) {


            Long num = (Long) businessChangeStatistic.get("num");
            if (num==0){
                return null;
            }
            Object radio = (Object)getRadio(businessNums, num);
            businessChangeStatistic.put("radio",radio);
        }

        return businessChangeStatistics;
    }

    @Override
    public List<Map<String, Object>> salesStatistic(String beginCreateTime, String endCreateTime) {
        List<Map<String, Object>> result = reportMpper.businessChangeStatistics(beginCreateTime, beginCreateTime);
        Integer businessNums = reportMpper.getBusinessNums(beginCreateTime, beginCreateTime);
        for (Map<String, Object> map : result) {
            Long num = (Long)map.get("num");
            BigDecimal radio = this.getRadio(businessNums, num);
            map.put("radio",radio);
        }
        return result;
    }
}