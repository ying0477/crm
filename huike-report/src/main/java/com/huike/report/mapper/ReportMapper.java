package com.huike.report.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import com.huike.clues.domain.vo.IndexStatisticsVo;

/**
 * 首页统计分析的Mapper
 * @author Administrator
 *
 */
public interface ReportMapper {
	/**=========================================基本数据========================================*/
	/**
	 * 获取线索数量
	 * @param beginCreateTime	开始时间
	 * @param endCreateTime		结束时间
	 * @param username			用户名
	 * @return
	 */
	Integer getCluesNum(@Param("startTime") String beginCreateTime,
						@Param("endTime") String endCreateTime,
						@Param("username") String username);

	/**
	 * 获取商机数量
	 * @param beginCreateTime	开始时间
	 * @param endCreateTime		结束时间
	 * @param username			用户名
	 * @return
	 */
	Integer getBusinessNum(@Param("startTime") String beginCreateTime,
						   @Param("endTime") String endCreateTime,
						   @Param("username") String username);

	/**
	 * 获取合同数量
	 * @param beginCreateTime	开始时间
	 * @param endCreateTime		结束时间
	 * @param username			用户名
	 * @return
	 */
	Integer getContractNum(@Param("startTime") String beginCreateTime,
						   @Param("endTime") String endCreateTime,
						   @Param("username") String username);

	/**
	 * 获取合同金额
	 * @param beginCreateTime	开始时间
	 * @param endCreateTime		结束时间
	 * @param username			用户名
	 * @return
	 */
	Double getSalesAmount(@Param("startTime") String beginCreateTime,
						  @Param("endTime") String endCreateTime,
						  @Param("username") String username);

	/**=========================================今日简报========================================*/



	/**=========================================待办========================================*/
	Integer getTodoCluesDGJ(@Param("startTime") String beginCreateTime,
						   @Param("endTime") String endCreateTime,
						   @Param("username") String username);

	Integer getTodoBusinessDGJ(@Param("startTime") String beginCreateTime,
							@Param("endTime") String endCreateTime,
							@Param("username") String username);

	Integer getTodoCluesDFP(@Param("startTime") String beginCreateTime,
							@Param("endTime") String endCreateTime,
							@Param("username") String username);

	Integer getTodoBusinessDFP(@Param("startTime") String beginCreateTime,
							@Param("endTime") String endCreateTime,
							@Param("username") String username);

	//****************************************饼状图
	List<Map<String,Object>> subjectStatistics(@Param("beginCreateTime")String beginCreateTime,@Param("endCreateTime") String endCreateTime);

	//*********************************************漏斗图
	Integer getBusinessNums(@Param("startTime") String beginCreateTime,
							@Param("endTime") String endCreateTime);

	Integer getCluesNums(@Param("startTime") String beginCreateTime,
							   @Param("endTime") String endCreateTime);

	Integer getContractNums(@Param("startTime") String beginCreateTime,
							@Param("endTime") String endCreateTime);

	Integer getEffectiveCluesNums(@Param("startTime") String beginCreateTime,
							   @Param("endTime") String endCreateTime);

	//**********************************************商机转化龙虎榜
	/*Long businessUserDo(@Param("UserName")String UserName);

	Integer getAllBusinessNums(@Param("beginCreateTime")String beginCreateTime,@Param("endCreateTime") String endCreateTime);

	List<Map<String,Object>> getUserName();*/

	List<Map<String,Object>> businessChangeStatistics(@Param("startTime") String beginCreateTime,
													  @Param("endTime") String endCreateTime);
	/**
	 * 商机转龙虎榜
	 * @param beginCreateTime
	 * @param endCreateTime
	 * @return
	 */
	List<Map<String,Object>> getBusinessChangeStatistics(@Param("beginCreateTime") String beginCreateTime,@Param("endCreateTime") String endCreateTime);
}
