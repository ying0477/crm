package com.huike.business.mapper;

import java.util.List;
import java.util.Map;

import com.huike.business.domain.TbBusiness;
import com.huike.business.domain.TbBusinessTrackRecord;
import com.huike.business.domain.vo.BusinessTrackVo;
import org.apache.ibatis.annotations.Param;

/**
 * 商机跟进记录Mapper接口
 * @date 2021-04-28
 */
public interface TbBusinessTrackRecordMapper {
    void update(TbBusiness tbBusiness);
    void insert(TbBusinessTrackRecord tbBusinessTrackRecord);
    List<Map<String,Object>> list(@Param("id") Long id);
}