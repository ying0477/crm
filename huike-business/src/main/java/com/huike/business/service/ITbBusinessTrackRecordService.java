package com.huike.business.service;

import com.huike.business.domain.vo.BusinessTrackVo;

import java.util.List;
import java.util.Map;

/**
 * 商机跟进记录Service接口
 * @date 2021-04-28
 */
public interface ITbBusinessTrackRecordService {
    void add(BusinessTrackVo businessTrackVo);
    List<Map<String,Object>> list(Long id);

}
