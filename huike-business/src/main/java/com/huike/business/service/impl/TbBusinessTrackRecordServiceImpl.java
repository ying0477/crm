package com.huike.business.service.impl;


import com.huike.business.domain.TbBusiness;
import com.huike.business.domain.TbBusinessTrackRecord;
import com.huike.business.domain.vo.BusinessTrackVo;
import com.huike.business.mapper.TbBusinessTrackRecordMapper;
import com.huike.business.service.ITbBusinessTrackRecordService;
import com.huike.clues.mapper.TbClueTrackRecordMapper;
import com.huike.common.utils.SecurityUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * 商机跟进记录Service业务层处理
 * 
 * @author wgl
 * @date 2021-04-28
 */
@Service
public class TbBusinessTrackRecordServiceImpl implements ITbBusinessTrackRecordService {
    @Autowired
    private TbBusinessTrackRecordMapper tbBusinessTrackRecordMapper;

    @Override
    public void add(BusinessTrackVo businessTrackVo) {
        TbBusiness tbBusiness = new TbBusiness();
        String username = SecurityUtils.getUsername();
        BeanUtils.copyProperties(businessTrackVo,tbBusiness);
        tbBusiness.setCreateBy(username);
        tbBusiness.setCreateTime(new Date());
        tbBusinessTrackRecordMapper.update(tbBusiness);
        TbBusinessTrackRecord tbBusinessTrackRecord = new TbBusinessTrackRecord();
        tbBusinessTrackRecord.setBusinessId(businessTrackVo.getBusinessId());
        tbBusinessTrackRecord.setCreateBy(username);
        tbBusinessTrackRecord.setRecord(businessTrackVo.getRecord());
        tbBusinessTrackRecord.setCreateTime(new Date());
        tbBusinessTrackRecord.setTrackStatus(businessTrackVo.getTrackStatus());
        tbBusinessTrackRecord.setNextTime(businessTrackVo.getNextTime());
        tbBusinessTrackRecordMapper.insert(tbBusinessTrackRecord);
    }

    @Override
    public List<Map<String, Object>> list(Long id) {
        List<Map<String, Object>> list = tbBusinessTrackRecordMapper.list(id);
        return list;
    }
}
