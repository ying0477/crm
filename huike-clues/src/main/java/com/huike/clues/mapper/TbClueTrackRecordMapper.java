package com.huike.clues.mapper;


import com.huike.clues.domain.TbClueTrackRecord;
import com.huike.clues.domain.vo.ClueTrackRecordVo;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * 线索跟进记录Mapper接口
 * @date 2021-04-19
 */
public interface TbClueTrackRecordMapper {

    void updateClub(@Param("tbClueTrackRecord") ClueTrackRecordVo tbClueTrackRecord);

    void addClubTrackRecord(@Param("tbClueTrackRecord")TbClueTrackRecord tbClueTrackRecord);

    List<Map<String,Object>> getRows(@Param("clueId")Long clueId);
}
