package com.huike.clues.service.impl;


import com.huike.clues.domain.TbClueTrackRecord;
import com.huike.clues.domain.vo.ClueTrackRecordVo;
import com.huike.clues.mapper.TbClueTrackRecordMapper;
import com.huike.clues.service.ITbClueTrackRecordService;
import com.huike.common.core.page.TableDataInfo;
import com.huike.common.utils.SecurityUtils;
import com.huike.common.utils.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.awt.*;
import java.util.List;
import java.util.Map;

/**
 * 线索跟进记录Service业务层处理
 * @date 2022-04-22
 */
@Service
public class TbClueTrackRecordServiceImpl implements ITbClueTrackRecordService {

    @Autowired
    private TbClueTrackRecordMapper tbClueTrackRecordMapper;

    @Override
    public void add(ClueTrackRecordVo tbClueTrackRecord) {
        tbClueTrackRecordMapper.updateClub(tbClueTrackRecord);
        //创建跟进实体类
        String username = SecurityUtils.getUsername();
        TbClueTrackRecord tbClueTrackRecord1 = new TbClueTrackRecord();
        tbClueTrackRecord1.setClueId(tbClueTrackRecord.getClueId());
        tbClueTrackRecord1.setCreateBy(username);
        tbClueTrackRecord1.setSubject(tbClueTrackRecord.getSubject());
        tbClueTrackRecord1.setRecord(tbClueTrackRecord.getRecord());
        tbClueTrackRecord1.setLevel(tbClueTrackRecord.getLevel());
        tbClueTrackRecord1.setType(tbClueTrackRecord.getType());
        tbClueTrackRecord1.setFalseReason(tbClueTrackRecord.getFalseReason());
        tbClueTrackRecord1.setNextTime(tbClueTrackRecord.getNextTime());
        //添加入表
        tbClueTrackRecordMapper.addClubTrackRecord(tbClueTrackRecord1);
    }

    @Override
    public TableDataInfo list(Long clueId) {
        TableDataInfo tableDataInfo = new TableDataInfo();
        List<Map<String,Object>> rows = tbClueTrackRecordMapper.getRows(clueId);
        tableDataInfo.setRows(rows);
        tableDataInfo.setCode(200);
        tableDataInfo.setMsg("查询成功");
        tableDataInfo.setParams(null);
        return tableDataInfo;
    }
}
